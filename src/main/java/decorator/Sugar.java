package decorator;

public class Sugar extends Condimentdecorator {

    private Beverage decorated;

    public Sugar(Beverage beverage) {
        decorated = beverage;
    }

    @Override
    public String getDescription() {
        String descOfDecorated = decorated.getDescription();
        return "Sugar with " + descOfDecorated;
    }

    @Override
    public Double getCost() {
        Double costOfDecorated = decorated.getCost();
        return costOfDecorated + 1;
    }
}
