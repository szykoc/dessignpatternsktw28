package decorator;

public abstract class Beverage {

    public String description = "Base Beverage";

    public String getDescription() {
        return description;
    }

    public abstract Double getCost();
}
