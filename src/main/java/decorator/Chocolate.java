package decorator;

public class Chocolate extends Condimentdecorator {

    private Beverage decorated;

    public Chocolate(Beverage beverage) {
        decorated = beverage;
    }

    @Override
    public String getDescription() {
        String descOfDecorated = decorated.getDescription();
        return "Chocolate with " + descOfDecorated;
    }

    @Override
    public Double getCost() {
        Double costOfDecorated = decorated.getCost();
        return costOfDecorated + 23.5;
    }
}
