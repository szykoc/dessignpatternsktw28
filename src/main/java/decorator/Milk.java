package decorator;

public class Milk extends Condimentdecorator {

    private Beverage decorated;

    public Milk(Beverage beverage) {
        decorated = beverage;
    }

    @Override
    public String getDescription() {
        String descOfDecorated = decorated.getDescription();
        return "Milk with " + descOfDecorated;
    }

    @Override
    public Double getCost() {
        Double costOfDecorated = decorated.getCost();
        return costOfDecorated + 0.5;
    }
}
