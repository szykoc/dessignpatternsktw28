package decorator;

import java.io.FileWriter;

public class Runner {


    public static void main(String[] args) {
        Beverage espresso = new Espresso();
        Beverage milkWithEspesso = new Milk(espresso);

        Double cost = milkWithEspesso.getCost();

        System.out.println(cost);

        String desc = milkWithEspesso.getDescription();
        System.out.println(desc);

        Double cost2 = new Milk(new Espresso()).getCost();

        System.out.println(cost2);

        Beverage espresoWithMilkikWIht =
        new Milk(
                new Sugar(
                        new Carmel(
                                new Chocolate
                                        (new Espresso()))));


        System.out.println(espresoWithMilkikWIht.getCost());


    }
}
