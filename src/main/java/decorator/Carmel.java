package decorator;

public class Carmel extends Condimentdecorator {

    private Beverage decorated;

    public Carmel(Beverage beverage) {
        decorated = beverage;
    }

    @Override
    public String getDescription() {
        String descOfDecorated = decorated.getDescription();
        return "Carmel with " + descOfDecorated;
    }

    @Override
    public Double getCost() {
        Double costOfDecorated = decorated.getCost();
        return costOfDecorated + 2;
    }
}
