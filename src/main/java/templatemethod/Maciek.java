package templatemethod;

public class Maciek extends GoToWork {

    @Override
    protected void breakfast() {
        System.out.println("Parówki");
    }

    @Override
    protected void driveTransport() {
        System.out.println("Autobus");
    }
}
