package templatemethod;

public class Runner {
    public static void main(String[] args) {
        GoToWork krzysiek = new Krzysiek();
        GoToWork maciek = new Maciek();

        krzysiek.goToWorkSchema();
        System.out.println("------");
        maciek.goToWorkSchema();
    }
}
