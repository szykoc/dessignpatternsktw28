package templatemethod;

public abstract class GoToWork {

    //template method
    public final void goToWorkSchema(){
        breakfast();
        driveTransport();
        accesToDoor();
    }

    private void accesToDoor() {
        System.out.println("Door opened");
    }

    protected abstract void breakfast();
    protected abstract void driveTransport();
}
