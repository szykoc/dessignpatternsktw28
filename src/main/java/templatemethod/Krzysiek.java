package templatemethod;

public class Krzysiek extends GoToWork {
    @Override
    protected void breakfast() {
        System.out.println("Jajko");
    }

    @Override
    protected void driveTransport() {
        System.out.println("Tramwaj");
    }
}
