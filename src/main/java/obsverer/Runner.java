package obsverer;

import java.math.BigDecimal;

public class Runner {
    public static void main(String[] args) {
        AuctionManager am = new AuctionManager();

        AuctionObserver company = new Company();
        am.addObserver(company);

        AuctionObserver user = new User();
        am.addObserver(user);

        am.changePrice(BigDecimal.TEN);

        am.removeObsever(company);

        am.changePrice(BigDecimal.valueOf(20.20));


    }
}
