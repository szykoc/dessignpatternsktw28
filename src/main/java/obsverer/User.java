package obsverer;

import java.math.BigDecimal;

public class User implements AuctionObserver {
    @Override
    public void inform(BigDecimal newPrice) {
        System.out.println("user new price: " + newPrice);
    }
}
