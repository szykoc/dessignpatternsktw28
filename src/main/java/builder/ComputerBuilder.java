package builder;

public interface ComputerBuilder {

    Computer build();

    ComputerBuilder processor(Double processor);

    ComputerBuilder ram(Integer ram);

    ComputerBuilder monitorSize(Double size);

    ComputerBuilder yearsOld(Integer yearsOld);

    ComputerBuilder weight(Double weight);

    ComputerBuilder operationSystem(String operationSystem);
}
