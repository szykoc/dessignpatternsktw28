package builder;

//DTO - Data Transfer Object

public class Computer {

    private Double processor;
    private Integer ram;
    private Double monitorSize;
    private Integer yearsOld;
    private Double weight;
    private String operationSystem;

    public Computer(Double processor, Integer ram) {
        this.processor = processor;
        this.ram = ram;
    }

    public Double getProcessor() {
        return processor;
    }

    public void setProcessor(Double processor) {
        this.processor = processor;
    }

    public Integer getRam() {
        return ram;
    }

    public void setRam(Integer ram) {
        this.ram = ram;
    }

    public Double getMonitorSize() {
        return monitorSize;
    }

    public void setMonitorSize(Double monitorSize) {
        this.monitorSize = monitorSize;
    }

    public Integer getYearsOld() {
        return yearsOld;
    }

    public void setYearsOld(Integer yearsOld) {
        this.yearsOld = yearsOld;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getOperationSystem() {
        return operationSystem;
    }

    public void setOperationSystem(String operationSystem) {
        this.operationSystem = operationSystem;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "processor=" + processor +
                ", ram=" + ram +
                ", monitorSize=" + monitorSize +
                ", yearsOld=" + yearsOld +
                ", weight=" + weight +
                ", operationSystem='" + operationSystem + '\'' +
                '}';
    }
}
