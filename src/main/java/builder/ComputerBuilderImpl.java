package builder;

public class ComputerBuilderImpl implements ComputerBuilder {

    private Computer computer;

    public ComputerBuilderImpl(Double processor, Integer ram) {
        computer = new Computer(processor, ram);
    }

    public Computer build() {
        return computer;
    }

    public ComputerBuilder processor(Double processor) {
        computer.setProcessor(processor);
        return this;
    }

    public ComputerBuilder ram(Integer ram) {
        return null;
    }

    public ComputerBuilder monitorSize(Double size) {
        return null;
    }

    public ComputerBuilder yearsOld(Integer yearsOld) {
        return null;
    }

    public ComputerBuilder weight(Double weight) {
        return null;
    }

    public ComputerBuilder operationSystem(String operationSystem) {
        return null;
    }
}
