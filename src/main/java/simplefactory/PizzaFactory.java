package simplefactory;

public class PizzaFactory {

    public static Pizza createPizza(PizzaType type) {
        switch (type) {
            case PEPERONI:
                return new PeperoniPizza();
            case HAWAIAN:
                return new HawaianPizza();
            default:
                throw new IllegalArgumentException("That type is not supported");
        }
    }
}
