package simplefactory;

public class PizzaShop {

    public Pizza orderPizza(PizzaType type){
        Pizza pizza = PizzaFactory.createPizza(type);
        pizza.bake();
        return pizza;
    }
}
