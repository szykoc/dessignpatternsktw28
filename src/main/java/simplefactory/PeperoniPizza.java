package simplefactory;

public class PeperoniPizza extends Pizza {
    public PeperoniPizza(){
        name = "Peperoni Pizza";
        price = 10.50;
        description = "Peperoni pizza with peperoni";
    }
}
