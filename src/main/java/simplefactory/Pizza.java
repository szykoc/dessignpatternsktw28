package simplefactory;

public abstract class Pizza {
    String name;
    String description;
    Double price;

    void bake() {
        System.out.println("Bake for 20 minutes");
    }
}
