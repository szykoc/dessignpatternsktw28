package simplefactory;

public class Runner {
    public static void main(String[] args) {
        PizzaShop pizzaShop = new PizzaShop();
        Pizza pizza = pizzaShop.orderPizza(PizzaType.PEPERONI);
        System.out.println(pizza.name + " " + pizza.description);

        Pizza pizza2 = pizzaShop.orderPizza(PizzaType.HAWAIAN);
        System.out.println(pizza2.name + " " + pizza2.description);

    }
}
