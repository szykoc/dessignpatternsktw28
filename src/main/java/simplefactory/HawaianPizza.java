package simplefactory;

public class HawaianPizza extends Pizza {
    public HawaianPizza(){
        name = "Hawaian Pizza";
        price = 12.50;
        description = "Hawaian pizza with peperoni";
    }
}
